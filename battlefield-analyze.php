<!DOCTYPE html>
<head>
	<meta charset="utf-8"/>
	<title>My Web Page</title>
	<style type="text/css">
	body{
		width: 760px; /* how wide to make your web page */
		background-color: teal; /* what color to make the background */
		margin: 0 auto;
		padding: 0;
		font:12px/16px Verdana, sans-serif; /* default font */
	}
	div#main{
		background-color: #FFF;
		margin: 0;
		padding: 10px;
	}
	</style>
</head>
<body><div id="main">
	<h1>Battlefield Analysis</h1>
	<h2>Latest Critiques</h2>
	<?php
	session_start(); 
	require 'database.php';
	$stmt = $mysqli->prepare("select battlefield.id, battlefield.critique from battlefield ORDER BY battlefield.id DESC 
		LIMIT 5");

	if(!$stmt){
		printf("Query Prep Failed: %s\n", $mysqli->error);
		exit;
	}
	$stmt->bind_param('is', $id, $critique);
	$stmt->execute();
	$stmt->bind_result($id, $critique);

	echo "<ul>\n";
	while($stmt->fetch()){
		printf("\t <li> %s <br> %s  \n",
			htmlspecialchars($id),
			htmlspecialchars($critique),
			$id,
			$critique
			);
	}
	echo "</ul>\n";
	$stmt->close();

	echo "Battle Statistics";
	$stmt2 = $mysqli->prepare("select avg(ammunition) as average_ammo_per_sec, battlefiled.ammunition, battlefield.soldiers from battlefield GROUP BY soldiers DESC ");
	if(!$stmt2){
		printf("Query Prep Failed: %s\n", $mysqli->error);
		exit;
	}
	$stmt->execute();
	$stmt->bind_result($soldiers, $average_ammo_per_sec);
	// while($stmt->fetch()){
	$stmt->close();

	?>

<a href="battlefield-submit.html">Submit a New Battle Report</a>

</div></body>
</html>